```
PURPOSE
       To make shell scripts easy to debug by:

       - Showing only output that requires human action.

       - Telling exactly where an error takes place.

USAGE
          On terminal

              so [-tag] [commands]

          On scripts

              so () {  /bin/so "-${FUNCNAME[1]}" "${@}"  }

              so [commands]

LEGEND
          [commands]
              The commands to execute, and to print along any error.

          [-tag]
              Optionally a line to show before any error.

NOTES
       - Danger: Hides any interactive screen.

       -  By  default the error message is the command stderr. If it is empty,
       stdout is used instead.

       - Of the error only the last 100 lines are printed.
